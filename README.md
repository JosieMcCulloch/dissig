dissig (dissimilarity significance)
====================================

What is it?
-----------
The purpose of this library is to find thresholds in a dissimilarity measure that mark a significant result. A strong and weak threshold is provided. Above the strong threshold, dissimilarity is always found to be significant. Below the weak threshold, dissimilarity is always found to be non-significant. Between these thresholds is the area of uncertianty in which dissimilarity may or may not be significant. This library uses permutation testing to statistically analyse the results of the measure

The 'results' directory stores the results for the user. This includes

* values of dissimilarity for all fuzzy set pairs in [function].csv
* results from permutation testing on all fuzzy set pairs in [function]res.csv
* figure showing a scatter plot of the dissimilarity and permutation test results in [function].png

where the function is provided by the user


How to use
----------
run: python main.py group function [subset_size]
group is 'L1', 'L2' or 'L3' correspond to 1) Gaussian, 2) bimodal and 3) multi-modal fuzzy sets
function is the dissimilarity function in the file compare_sets.py
subset_size (optional) limits the discretisation of the fuzzy sets (choose <300)

To test additional dissimilarity measures, add the function to compare_sets.py


The Latest Version
------------------
The latest version can found at
https://bitbucket.org/JosieMcCulloch/dissig


Prerequisites
--------------
* python 2.7
* decimal
* matplotlib


Installation
------------
No installation is required.


Contacts
--------
If you wish to contact about queries or submit bug reports,
please contact the mail address: josie.mcculloch@nottingham.ac.uk.


License
-------
Copyright (C) 2018 Josie McCulloch

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
