"""This module is used to run permutation testing for a given group of
fuzzy sets and a given dissimilarity method.
"""


import csv
import logging
import pickle
from collections import defaultdict
from random import random
from decimal import Decimal
import matplotlib.pyplot as plt


from discrete_t1_fuzzy_set import DiscreteT1FuzzySet
import compare_sets


DECIMAL_ROUNDING = Decimal(10) ** -4
TOTAL_RESAMPLES = 1000
ALPHA_CRITERION = 0.01


def load_sets(filename):
    """Load fuzzy sets using pickle from given filename."""
    fsock = open(filename, 'rb')
    try:
        sets = pickle.load(fsock)
    finally:
        fsock.close()
    return sets


def save_csv(data, row_length, filename):
    """Save the results in to a csv file."""
    with open('results/' + filename + '.csv', 'w') as csvfile:
        wr = csv.writer(csvfile)
        start = 0
        for i in range(row_length):
            row = ['' for j in range(i)]
            row.extend(data[start:start+row_length])
            wr.writerow(row)
            start += row_length
            row_length -= 1


def plot_scatter(res_results, d_results):
    """Create a scatter plot of the results from the given files."""
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.scatter(res_results, d_results, s=20, alpha=0.8)
    ax.set_xlabel(r'$p$', fontsize=30)
    #set the tick positions to remove them from the top and right
    ax.xaxis.set_ticks_position('bottom')
    ax.yaxis.set_ticks_position('left')
    ax.tick_params(axis='x', labelsize=18)
    ax.tick_params(axis='y', labelsize=18)
    ax.axes.set_xlim(-0.01, 1.01)
    ax.axes.set_ylim(0.0, 1)
    fig.subplots_adjust(bottom=0.15)
    return plt


def create_subset(fs, subset_size):
    """Create a subset of information from the fuzzy set."""
    pool = []
    for k, v in fs.points.items():
        for alpha in range(int(v * 10)):
            pool.append(k)
    new_points = defaultdict(int)
    for i in range(min(subset_size, len(pool))):
        new_points[pool.pop(int(random() * len(pool)))] += 0.1
    return DiscreteT1FuzzySet(new_points)


def resample_pair(set1, set2, d_function):
    """Run a single resampling test for two fuzzy sets."""
    pool = []
    for fs in (set1, set2):
        for k, v in fs.points.items():
            for alpha in range(int(v * 10)):
                pool.append(k)
    new_sets = [defaultdict(int), defaultdict(int)]
    cur_set = 0
    while len(pool) != 0:
        value = pool.pop(int(random() * len(pool)))
        if new_sets[cur_set][value] + 0.1 > 1:
            new_sets[(cur_set + 1) % 2][value] += 0.1
        else:
            new_sets[cur_set][value] += 0.1
            cur_set = (cur_set + 1) % 2
    fs1 = DiscreteT1FuzzySet(new_sets[0])
    fs2 = DiscreteT1FuzzySet(new_sets[1])
    return d_function(fs1, fs2)


def resample_all(group, d_function, subset_size=None):
    """Run permutation testing for all sets of a given type and method.

    group: L1, L2 or L3
    d_function: name of the similarity function in module 'compare_sets'
    subset_size: reduce the level of  discretisation in the fuzzy sets
    """
    fuzzy_sets = load_sets('sets/' + group + '.pickle')
    total_sets = len(fuzzy_sets)
    res_results = []
    d_results = []
    #reduce to a subset to test different sample sizes
    if subset_size is not None:
        new_fuzzy = []
        for fs in fuzzy_sets:
            new_fuzzy.append(create_subset(fs, int(subset_size)))
        fuzzy_sets = new_fuzzy
    for i in range(total_sets):
        if i % 10 == 0:
            logging.info(i)
        for j in range(i, total_sets):
            dissim = d_function(fuzzy_sets[i], fuzzy_sets[j])
            d_results.append(dissim.quantize(DECIMAL_ROUNDING))
            total_higher = 0
            for res_index in range(TOTAL_RESAMPLES):
                res = resample_pair(fuzzy_sets[i], fuzzy_sets[j], d_function)
                if res > dissim:
                    total_higher += 1
            res_results.append(round(total_higher/float(TOTAL_RESAMPLES), 4))
    save_csv(res_results, total_sets, group + d_function.__name__+'res')
    save_csv(d_results, total_sets, group + d_function.__name__)
    plt = plot_scatter(res_results, d_results)
    plt.savefig('results/' + group + d_function.__name__ + '.png')
    find_thresholds(d_results, res_results)


def find_thresholds(d_results, res_results, alpha=0.01):
    """Calculate weak and strong threshold."""
    lowest_sig = 1.0
    highest_sig = 0.0
    for i in range(len(d_results)):
        if res_results[i] >= alpha:
            highest_sig = max(highest_sig, d_results[i])
        else:
            lowest_sig = min(lowest_sig, d_results[i])
    logging.info('weak threshold:' + str(round(lowest_sig, 4)))
    logging.info('strong threshold:' + str(round(highest_sig, 4)))


def run_and_log(group, d_function, subset_size=None):
    """Generate dissimilarity and permutation results and log the progress."""
    logging.basicConfig(level=logging.DEBUG,
                        filename="logfile",
                        filemode="a+",
                        format="%(asctime)-15s %(message)s")
    logging.info('Group is ' + group)
    logging.info('Dissimilarity function is ' + d_function.__name__)
    logging.info('Subset_size = ' + str(subset_size))
    resample_all(group, d_function, subset_size)
    logging.info('Finished')
