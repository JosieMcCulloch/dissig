"""This module is used to create a discrete type-1 fuzzy set."""

from decimal import Decimal


DECIMAL_ROUNDING = Decimal(10) ** -4


class DiscreteT1FuzzySet():
    """Create a discrete type-1 fuzzy set."""

    def __init__(self, points):
        """Create a discrete type-1 fuzzy set using a dict of x,mu pairs."""
        self.points = points
        self.x_min = min(points.keys())
        self.x_max = max(points.keys())

    def calculate_membership(self, x):
        """Calculate the membership of x within the uod.

        Returns a Decimal value.
        """
        try:
            return Decimal(self.points[x]).quantize(DECIMAL_ROUNDING)
        except KeyError:
            return 0

