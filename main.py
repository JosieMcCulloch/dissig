"""This module is used to run permutation testing for a given group of
fuzzy sets and a given dissimilarity method.

Note that the test may take several hours to run.
Progress is recorded in 'logfile' within the working directory.

Parameters:
group: L1, L2 or L3
d_function: name of the dissimilarity function in module 'compare_sets'
subset_size: use to reduce the level of discretisation in the fuzzy sets
"""


import sys
import dissig
import compare_sets


if __name__ == '__main__':
    group, measure = sys.argv[1:3]
    try:
        subset_size = sys.argv[3]
    except:
        subset_size = None
    dissig.run_and_log(group, compare_sets.__dict__[measure], subset_size)
