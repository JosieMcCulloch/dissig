"""This module contains functions to calculate dissimilarity between fuzzy sets."""

from decimal import Decimal


def _get_all_x_points(fs1, fs2):
    """Get the union of the discretised x points used by the sets."""
    return list(set(fs1.points.keys()).union(set(fs2.points.keys())))


def jaccard(fs1, fs2):
    """Calculate the jaccard dissimilarity for fuzzy sets fs1 and fs2."""
    upper = 0
    lower = 0
    for x in _get_all_x_points(fs1, fs2):
        mu1 = fs1.calculate_membership(x)
        mu2 = fs2.calculate_membership(x)
        upper += min(mu1, mu2)
        lower += max(mu1, mu2)
    return 1 - (upper / lower)


def dice(fs1, fs2):
    """Calculate the dice dissimilarity for fuzzy sets fs1 and fs2."""
    upper = 0
    lower = 0
    for x in _get_all_x_points(fs1, fs2):
        mu1 = fs1.calculate_membership(x)
        mu2 = fs2.calculate_membership(x)
        upper += 2 * min(mu1, mu2)
        lower += mu1 + mu2
    return 1 - (upper / lower)


def chen(fs1, fs2):
    """Ratio between the product of memberships and the cardinality."""
    top = 0
    fs1_squares = 0
    fs2_squares = 0
    for x in _get_all_x_points(fs1, fs2):
        y1 = fs1.calculate_membership(x)
        y2 = fs2.calculate_membership(x)
        top += (y1 * y2)
        fs1_squares += (y1 * y1)
        fs2_squares += (y2 * y2)
    return 1 - (top / max(fs1_squares, fs2_squares))


def pappis2(fs1, fs2):
    """The ratio between the negation and addition of membership values."""
    dist1 = 0
    dist2 = 0
    for x in _get_all_x_points(fs1, fs2):
        y1 = fs1.calculate_membership(x)
        y2 = fs2.calculate_membership(x)
        dist1 += abs(y1 - y2)
        dist2 += y1 + y2
    return (dist1 / dist2)


def zwick(fs1, fs2):
    """The maximum membership of the intersection of the fuzzy sets."""
    sim = Decimal(0)
    for x in _get_all_x_points(fs1, fs2):
        y1 = fs1.calculate_membership(x)
        y2 = fs2.calculate_membership(x)
        sim = max(sim, min(y1, y2))
    return 1 - sim


def pappis1(fs1, fs2):
    """Based on the maximum distance between membership values."""
    dist = Decimal(0)
    for x in _get_all_x_points(fs1, fs2):
        y1 = fs1.calculate_membership(x)
        y2 = fs2.calculate_membership(x)
        dist = max(dist, abs(y1 - y2))
    return dist
